
// -----------------------------------
// SERVER ENTRYPOINT - ES6 Transpile
// -----------------------------------
require('babel-register')({
  presets: ['es2015', 'react', 'stage-2']
});

require('./main');

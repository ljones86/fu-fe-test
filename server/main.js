const webpack = require('webpack');
const webpackConfig = require('../webpack.config');
const compiler = webpack(webpackConfig);

import fs from 'fs'
const path = require('path');
const express = require('express');
const bodyParser = require('body-parser');
const http = require('http');
const fetch = require('node-fetch');

import React from 'react';
import ReactDOMServer from 'react-dom/server'
const renderToString = require('react-dom/server');
const classnames = require('classnames');

const webpackDevMiddleware = require('webpack-dev-middleware');
const webpackHotMiddleware = require('webpack-hot-middleware');

import App from '../src/App'

const app = express();
const server = http.Server(app);
const port = 3000;

const renderApp = (req, res) => {
  const html = ReactDOMServer.renderToString(<App />);

  fs.readFile('./index.html', 'utf8', function (err, data) {
    if (err) throw err;
    const document = data.replace(/<div id="root"><\/div>/, `<div id="root">${html}</div>`);
    res.send(document);
  });
}

app.use((req,res,next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.use('/dist', express.static('dist'));

app.get('/', renderApp);

app.listen(port, () => console.log('Running!'));

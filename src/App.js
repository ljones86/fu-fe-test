import React, { Component } from 'react';
import 'es6-promise/auto';
import 'isomorphic-fetch';

class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      characterListMounted: false,
      characterList: undefined
    }
  }

  componentWillMount() {
    fetch('https://swapi.co/api/people/')
    .then((res) => (
      res.json()
    )).then((body) => {
      if (this.refs.container)
        this.setState({characterList: body.results})
    })
  }

	render() {
    let characterList = this.state.characterList
    let renderCharacterList = characterList !== undefined ? characterList.map((item,key) =>
      <ul key={key} className="list-group">
        <li className="list-group-item text-muted">Name: <span className="text-info">{item.name}</span></li>
        <li className="list-group-item text-muted">height: <span className="text-info">{item.height}</span>cm</li>
        <li className="list-group-item text-muted">hair color: <span className="text-info">{item.hair_color}</span></li>
        <li className="list-group-item text-muted">eye color: <span className="text-info">{item.eye_color}</span></li>
        <li className="list-group-item text-muted">skin color: <span className="text-info">{item.skin_color}</span></li>
        <li className="list-group-item text-muted">birth year: <span className="text-info">{item.birth_year}</span></li>
        <li className="list-group-item text-muted">gender: <span className="text-info">{item.gender}</span></li>
      </ul>
    )
    :
    '';

		return (
			<div className="container" ref="container">
        <div className="row">
          <div className="col-xs-12">
            { renderCharacterList }
          </div>
        </div>
      </div>
		)
	}
};

module.exports = App

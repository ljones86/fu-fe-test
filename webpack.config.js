
const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

var config = {
     entry: ['react-hot-loader/patch',
            path.join(__dirname,
            './src/index.js')
            ],
     output: {
         path: path.join(__dirname, 'dist'),
         filename: 'bundle.js'
     },
     module: {
     	rules: [
     		{
                test: /\.(js|jsx)?$/,
                use: [{
                    loader: 'babel-loader',
                    options: {
                        presets: ['es2015', 'react', 'stage-2']
                    },
                }],
                exclude: '/node_modules/',
                include: path.join(__dirname, 'src')
            },
             {
                test: /\.scss?$/,
                use: ExtractTextPlugin.extract({
                    use: [
                    {loader:'css-loader'},
                    {loader: 'sass-loader'},
                    ],
                    fallback: "style-loader",
                  }),
            },
            {
                test: /\.(eot|svg|ttf|woff|woff2)$/,
                use: [{loader:'file-loader?name=src/public/fonts/[name].[ext]'}]
            },
        ]
     },
     plugins: [
        new webpack.optimize.OccurrenceOrderPlugin(),
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NoEmitOnErrorsPlugin()
     ],
     resolve: {
        extensions: ['.js', '.jsx', '.json', '.css', '.scss'],
        alias: {
            styles: path.join(__dirname, 'src/styles'),
            fonts: path.join(__dirname, 'src/fonts')
        },
    },
 }
 module.exports = config;
